﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour {

    Rigidbody enemyBody,player1Body,player2Body;
    bool followP1;
    GameObject enemyObject, player1Object, player2Object;
    int nrOfHits = 0;
    Animator anim;
    private AudioSource audio;
    
    AudioClip clip;
	void Start () {
        enemyObject =this.gameObject;
        player1Object = GameObject.FindGameObjectWithTag("Player1");
        player2Object = GameObject.FindGameObjectWithTag("Player2");
        enemyBody = enemyObject.GetComponent<Rigidbody>();
        player1Body = player1Object.GetComponent<Rigidbody>();
        player2Body = player2Object.GetComponent<Rigidbody>();
        anim = this.GetComponent<Animator>();
        audio = this.GetComponent<AudioSource>();
        audio.volume = 0.1f;
	}
	
    void followPlayer()
    {
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
        {
            if (Vector3.Distance(this.enemyBody.position, player1Body.position) > Vector3.Distance(this.enemyBody.position, player2Body.position))
            {
                enemyBody.MovePosition(Vector3.MoveTowards(enemyBody.position, player2Body.transform.position, 22f * Time.deltaTime));
                transform.LookAt(player2Body.transform);
            }
            else
            {
                enemyBody.MovePosition(Vector3.MoveTowards(enemyBody.position, player1Body.transform.position, 22f * Time.deltaTime));
                transform.LookAt(player1Body.transform);
            }
        }
    }
	
	void Update () {
        followPlayer();
        
	}

    void OnCollisionEnter(Collision col)
    {
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
        {
            if (col.gameObject.tag == "Player1" || col.gameObject.tag == "Player2")
            {
                if (col.gameObject.tag == "Player1")
                {
                    clip = (AudioClip)Resources.Load("0_PlayerMaleHit_7");
                }
                else
                {
                    clip = (AudioClip)Resources.Load("0_PlayerFemaleHit_6");
                }
                audio.clip = clip;
                if (!audio.isPlaying)
                {
                    audio.PlayOneShot(clip);
                }


                anim.Play("Die");

                player1Behavior.life--;

                Invoke("destroy", 2);
                Debug.Log(player1Behavior.life);
            }
            if (col.gameObject.tag == "Player1" && player1Behavior.life < 1 || col.gameObject.tag == "Player2" && player1Behavior.life < 1)
            {
                anim.Play("Die");

                Invoke("destroy", 3);
                Invoke("destroy2", 2);
            }

        }
    }

    void destroy()
    {
        DestroyObject(enemyObject);
    }

    void destroy2()
    {
        DestroyObject(GameObject.FindGameObjectWithTag("Player1"));
        DestroyObject(GameObject.FindGameObjectWithTag("Player2"));
        SceneManager.LoadScene("test");
    }


}
