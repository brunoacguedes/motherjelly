﻿using UnityEngine;
using System.Collections;

public class Axe : MonoBehaviour {
    AudioSource audio;
    AudioClip clip;
    Animator anim;
    Collider thisCol;
    void Start()
    {
        audio = this.GetComponent<AudioSource>();
        
    }
    
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            thisCol = col;
            anim = col.gameObject.GetComponent<Animator>();
            anim.Play("Die");
            clip = (AudioClip)Resources.Load("1_EnemyHit_6");
            audio.clip = clip;
            if (!audio.isPlaying)
            {
                audio.PlayOneShot(clip);
            }
            Debug.Log("Matou");
            Invoke("destroy",3);
            
        }
    }

    void destroy()
    {
        DestroyObject(thisCol.gameObject);
    }
 
}
