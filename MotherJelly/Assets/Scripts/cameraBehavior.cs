﻿using UnityEngine;
using System.Collections;

public class cameraBehavior : MonoBehaviour
{

    private GameObject player1, player2;
    private Vector3 offset;
    private string guiString,life;
    Vector3 pontoMedio;
    float auxF = 0;
    void Start()
    {
        player1 = GameObject.FindGameObjectWithTag("Player1");
        player2 = GameObject.FindGameObjectWithTag("Player2");
        offset = new Vector3(50, 0, 50);
        InvokeRepeating("generateEnemy", 10, 3f);
    }

    void Update()
    {
        pontoMedio = (player1.transform.position + player2.transform.position) / 2;
        pontoMedio.y = Mathf.Abs((player1.transform.position.x - player2.transform.position.x)) + Mathf.Abs((player1.transform.position.z - player2.transform.position.z));
        if (pontoMedio.y <= 45f)
            pontoMedio.y = 45f;
        transform.position = pontoMedio - offset;
        transform.LookAt((player1.transform.position + player2.transform.position) / 2);

        Invoke("callT", 1);

        guiString = "Score:  " +Mathf.RoundToInt(auxF).ToString();
        if (player1Behavior.life < 0)
        {
            life = "Lifes:0";
        }
        else{
              life = "Lifes:" + player1Behavior.life;
        }
      
        
    }

    void callT()
    {
        auxF += 1 / Mathf.Abs((Vector3.Distance(player1.transform.position, player2.transform.position)));
    }

    void generateEnemy()
    {
        if (Random.value >0.3f - (auxF / 500))
        {
            for (int i = 0; i < Random.value * 5; i++)
            {
                GameObject go = (GameObject)Instantiate(Resources.Load("enemy_prefab"), new Vector3(pontoMedio.x + Random.value * 30, 30f, Random.value * 30), Quaternion.identity);
            }
        }
    }

    void OnGUI()
    {
        GUIStyle myStyle = new GUIStyle();
        Font font = (Font)Resources.Load("Hana");
        myStyle.font = font;
        myStyle.fontSize = 28;
        myStyle.normal.textColor = Color.white;
        GUI.Label(new Rect(10, 10, 100, 20), guiString, myStyle);
        GUI.Label(new Rect(Camera.main.pixelWidth-100, 10, 100, 20), life, myStyle);
    }
}
