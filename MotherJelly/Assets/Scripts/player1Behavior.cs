﻿using UnityEngine;
using System.Collections;

public class player1Behavior : MonoBehaviour {

    public float speed;
    private GameObject player1;
    private float playerY = 0.5f;
    private Vector3 movement;
    private Quaternion rotation;
    private Animator playerAnimator;
    public static  int life = 3;
    private AudioSource audio;
    private bool isAttacking = false;
    AudioClip clip;
    void Start()
    {

        player1 = GameObject.FindGameObjectWithTag("Player1");
        playerAnimator = player1.GetComponent<Animator>();
        audio = player1.GetComponent<AudioSource>();
        audio.volume = 0.1f;
        life = 3;
    }

    private void movePlayer()
    {
        
        float moveHorizontal = Input.GetAxis("Horizontal2");
        float moveVertical = Input.GetAxis("Vertical2");
        movement = new Vector3(moveHorizontal * 6, 0f, moveVertical * 6);
        rotation = new Quaternion(moveHorizontal, 0, 0, 0);
        player1.GetComponent<Rigidbody>().AddForce(movement * speed);
        player1.transform.LookAt(new Vector3(player1.transform.position.x + moveHorizontal, player1.transform.position.y, player1.transform.position.z + moveVertical));
        if (Mathf.Abs(player1.GetComponent<Rigidbody>().velocity.x) > 0.5f || Mathf.Abs(player1.GetComponent<Rigidbody>().velocity.z) > 0.5f)
        {

            if (!playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("SPINATACK"))
            {
               
                playerAnimator.Play("walk");

                
               
            }
        }
        else
        {
               
                if (!playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("SPINATACK"))
                {
                  
                    playerAnimator.Play("idle");
                }
           
        }
       

    }
    void audioStop()
    {
        audio.Stop();
    }
    private void playerActions2()
    {
        //A
        if (Input.GetKey("joystick 2 button 1") && player1.GetComponent<Rigidbody>().transform.position.y <= 2.5f)
        {
            player1.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 25f, 0f) * speed);

           

                if (!playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("SPINATACK"))
                {

                    playerAnimator.Play("jump_001");
                    clip = (AudioClip)Resources.Load("0_PlayerJump");
                    audio.clip = clip;
                    if (!audio.isPlaying||audio.clip.name != "0_PlayerJump")
                    {
                        audio.PlayOneShot(clip);
                    }
                }
           
        }
        //B
        if (Input.GetKeyDown("joystick 2 button 0"))
        {
            playerAnimator.Play("SPINATACK");
            clip = (AudioClip)Resources.Load("0_Axe_Double");
            audio.clip = clip;
            audio.volume = 1f;
            audio.pitch = 0.5f;
            if (!audio.isPlaying || audio.clip.name != "0_Axe_Double")
            {
                audio.PlayOneShot(clip);
            }
            audio.volume = 0.5f;
            audio.pitch = 1f;
        }
    }
    private void playerActions()
    {
        //A
        if (Input.GetKey("joystick 2 button 0") && player1.GetComponent<Rigidbody>().transform.position.y <= 2.5f)
        {
            player1.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 25f, 0f) * speed);
            playerAnimator.Play("jump_001");
        }
        //B
        if (Input.GetKeyDown("joystick 2 button 1"))
        {
            //player2.GetComponent<Rigidbody>().AddForce(new Vector3(2000f, 0f, 0f));
        }

    }

    void FixedUpdate()
    {
        playerActions2();
        movePlayer();
    }
   
}
