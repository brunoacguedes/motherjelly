﻿using UnityEngine;
using System.Collections;

public class player2Behavior : MonoBehaviour {

    public float speed;
    private GameObject player2,player1;
    private float playerY = 0.5f,timer=0;
    private Vector3 movement;
    private Animator playerAnimator;
    private int life;
    private bool isAttacking = false;
    AudioClip clip;
    AudioSource audio;
    void Start()
    {
        player2 = GameObject.FindGameObjectWithTag("Player2");
        player1 = GameObject.FindGameObjectWithTag("Player1");
        playerAnimator = player2.GetComponent<Animator>();
        audio = this.gameObject.GetComponent<AudioSource>();

    }

    private void movePlayer()
    {
        float moveHorizontal = Input.GetAxis("Horizontal1");
        float moveVertical = Input.GetAxis("Vertical1");
        movement = new Vector3(moveHorizontal*6, 0f, moveVertical*6);
        player2.GetComponent<Rigidbody>().AddForce(movement*speed);
        player2.transform.LookAt(new Vector3(player2.transform.position.x + moveHorizontal, player2.transform.position.y, player2.transform.position.z + moveVertical));
        if(  Mathf.Abs(player2.GetComponent<Rigidbody>().velocity.x)>0.5f ||Mathf.Abs( player2.GetComponent<Rigidbody>().velocity.z)>0.5f )
        {
            if(!isAttacking)
            playerAnimator.Play("walk");
        }
        else
        {
            if (!isAttacking)
            playerAnimator.Play("idle");
        }
        
    }

    private void playerActions()
    {
        //A
        if (Input.GetKey("joystick 8 button 1") && player2.GetComponent<Rigidbody>().transform.position.y <= 2.5f)
        {
            player2.GetComponent<Rigidbody>().AddForce(new Vector3(0f,25f, 0f) * speed);
            if (!isAttacking)
            playerAnimator.Play("jump_001");
            clip = (AudioClip)Resources.Load("0_PlayerJump");
            audio.clip = clip;
            if (!audio.isPlaying || audio.clip.name != "0_PlayerJump")
            {
                audio.PlayOneShot(clip);
            }
        }
        //B
        if (Input.GetKeyDown("joystick 8 button 0"))
        {
           
            if (timer <= Time.time)
            {
                player1.GetComponent<Rigidbody>().AddForce(-(player1.GetComponent<Rigidbody>().position - player2.GetComponent<Rigidbody>().position) * 2000f * Time.smoothDeltaTime, ForceMode.Acceleration);
                timer = Time.time + 5;
            }
        }
    }

    void FixedUpdate()
    {
        playerActions();
        movePlayer();
        life = player1Behavior.life;
    }
}
